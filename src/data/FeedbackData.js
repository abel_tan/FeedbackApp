const FeedbackData = [
    {
        id: 1,
        rating: 10,
        text: "data text #1"
    },
    {
        id: 2,
        rating: 20,
        text: "data text #2"
    },
    {
        id: 3,
        rating: 30,
        text: "data text #3"
    }
]

export default FeedbackData