import spinner from '../assets/spinner.gif' 

function Spinner() {
  return (
    <img src={spinner} 
    alt="Loading..." 
    style={{width: '300px', margin: 'auto', display: 'auto'}} />
  )
}

export default Spinner